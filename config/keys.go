package config

// The following are all the viper keys
const (
	BuildType = "Build.type"

	AppEnv = "App.env"

	// DevelopmentMode is the viper key to indicate if running in development mode which allows for different functionality
	// running locally for development versus production like environments.
	DevelopmentMode = "DevMode"

	// Version is the viper key for the version of the application set at build time
	Version = "version"

	// BuildDate is the viper key for the date and time the service was built
	BuildDate = "builddate"

	// LogFormat is the viper key for the log format which indicates how logs should be formatted, json or for the console
	LogFormat = "Log.format"

	// LogLevel is the viper key for the level at which log entries should actually be written: debug, info, warn, or
	// error any log entries at a lower level are not written
	LogLevel = "Log.level"

	// LogSamplingRate is the viper key for how many of the same log messages within a second should trigger sampling/
	// If sampling is triggered then only every LogSampleEvery messages are logged. If the value is <= 0 sampling is
	// disabled.
	LogSamplingRate = "Log.SamplingRate"

	// LogSamplingInitial is the viper key for how many of the same log messages within a second should trigger sampling/
	// If sampling is triggered then only every LogSampleThereafter messages are logged. If the value is <= 0 sampling is
	// disabled.
	LogSamplingInitial = "Log.SamplingInitial"

	// LogSampleThereafter is the viper key for how many x messages should be logged once sampling is triggered.  If the value is
	// <= 0 it will be set to 50
	LogSampleThereafter = "Log.Samplethereafter"

	// LogSampleEvery is the viper key for how many x messages should be logged once sampling is triggered.  If the value is
	// <= 0 it will be set to 50
	LogSampleEvery = "Log.SampleEvery"

	// LogFX is the viper key for whether fx logging is enabled. FX invoke errors are always logged but to reduce chatter
	// general fx logging can be turned off.
	LogFX = "Log.fx"

	// LogHttpRequestLevel is the viper key for controlling what level of incoming http request level logging should be done.
	// ALL indicates all requests both start and request completion are logged
	// COMPLETION indicates all request completions are logged but no request starts
	// ERROR indicates only requests whose response code is greater than or equal to the LogHttpRequestErrorCodeThreshold
	//       are logged or whose duration is greater than or equal to the LogHttpRequestErrorDurationThreshold
	// NONE disabled all incoming http request logging
	LogHttpRequestLevel = "Log.HttpReqLevel"

	// LogHttpRequestErrorCodeThreshold is the viper key for controlling what http response codes get logged when the
	// LogHttpRequestLevel is ERROR, in that case any response codes >= to this setting are logged.
	LogHttpRequestErrorCodeThreshold = "Log.HttpReqErrCodeThreshold"

	// LogHttpRequestErrorDurationThreshold is the viper key for controlling what request durations are considered an
	// error and therefore should be logged if the LogHttpRequestLevel is ERROR.  Any requests greater than or equal to
	// this duration will be logged.
	LogHttpRequestErrorDurationThreshold = "Log.HttpReqErrDurThreshold"

	// ListenAddress is the viper key for the host/address to listen on
	ListenAddress = "Server.ListenAddress"

	// ListenPort is the viper key for the tcp port to listen on
	ListenPort = "Server.ListenPort"

	// InstalledVersion is the viper key for the installed version of the service
	InstalledVersion = "App.installedVersion"

	// InstallingVersion is the viper key for the version being installed
	InstallingVersion = "App.installingVersion"

	// InstallationStep is the viper key for the current installation step
	InstallationStep = "App.installationStep"

	RedisAddr = "Services.Redis.Addr"

	RedisPass = "Services.Redis.Pass"

	RedisPort = "Services.Redis.Port"

	IdentityAddress = "Services.Identity.Address"

	// GoogleOAuth2ClientId is the viper key for the `client_id` used in the Google oauth2 flow
	GoogleOAuth2ClientId = "Google.OauthClientId"

	// GoogleOAuth2ClientSecret is the viper key for the `client_secret` used in the Google oauth2 flow
	GoogleOAuth2ClientSecret = "Google.OauthClientSecret"

	// GoogleOAuth2RedirectUrl is the viper key for the `redirect_uri` used in the Google oauth2 flow
	GoogleOAuth2RedirectUrl = "Google.RedirectUrl"

	// GoogleOAuth2Scopes is the viper key for the `scopes` used in the Google oauth2 flow
	GoogleOAuth2Scopes = "Google.OauthScopes"

	SessionCookieName = "Cookies.Session.Name"

	StateCookieName = "Cookies.State.Name"

	DomainName = "Session.Domain"
)
