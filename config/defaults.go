package config

//go:generate go run config_embedder.go

import (
	"bytes"
	"fmt"
	"os"

	"github.com/spf13/viper"
)

const defaultsFile = "defaults"
const configFileType = "yaml"
const configFileExtension = ".yaml"

func LoadConfig(cfg *viper.Viper) {
	manager := NewViperConfigManager()
	cfg.SetConfigType(configFileType)
	//Load Defaults
	file, err := manager.GetString(defaultsFile + configFileExtension)
	if err != nil {
		os.Exit(1)
	}
	//Load defaults configuration file, if one doesn't exist, ignore it.
	err = cfg.ReadConfig(bytes.NewBuffer([]byte(file)))
	if err != nil {
		fmt.Printf("error: %v", err)
	}
	//---------------------------------------------------
	env := os.Getenv(EnvAppEnv.Name)
	file, err = manager.GetString(env + configFileExtension)
	if err == nil {
		// Read in configuration for the environment
		err = cfg.ReadConfig(bytes.NewBuffer([]byte(file)))
		if err != nil {
			os.Exit(1)
		}
	}

}
