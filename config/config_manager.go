package config

import (
	"fmt"
	"io/ioutil"
)

type ViperConfigManager interface {
	GetString(name string) (string, error)
}

func NewViperConfigManager() ViperConfigManager {
	if len(ViperCfg.Data) > 0 {
		return ViperCfg
	} else {
		return &manager{
			paths: []string{"./config/env/", "../../config/env/", "../../../config/env/"},
		}
	}
}

type manager struct {
	paths []string
}

func (f *manager) GetString(name string) (file string, err error) {
	var d []byte
	for _, path := range f.paths {
		/* #nosec */
		if d, err = ioutil.ReadFile(path + name); err != nil {
			continue
		} else {
			return string(d), nil
		}
	}
	return "", err
}

type embeddedManager struct {
	Data map[string]string
}

func (m *embeddedManager) Add(name string, data []byte) {
	m.Data[name] = string(data)
}

func (m *embeddedManager) GetString(name string) (string, error) {
	if data, ok := m.Data["env/"+name]; ok {
		return data, nil
	} else {
		return "", fmt.Errorf("no config file '%v' was embedded", name)
	}
}

func newEmbeddedManager() *embeddedManager {
	return &embeddedManager{
		Data: make(map[string]string),
	}
}

var ViperCfg = newEmbeddedManager()
