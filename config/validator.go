package config

// Validator is an interface implemented to validate configuration for a given component during start up before
// things are running thus giving the opportunity to fail fast.
type Validator interface {
	Validate() error
}
