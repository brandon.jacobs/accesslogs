package config

// EnvVar represents a environment variable being used and what viper setting key it is tied to along with
// description and other meta data to enable help output
type EnvVar struct {
	Name        string
	ViperKey    string
	Description string
}

var EnvAppEnv = EnvVar{
	Name:        "APP_ENV",
	ViperKey:    AppEnv,
	Description: "The application environment the build is running in, i.e. stage, pipeline, prod, local",
}

var EnvBuildType = EnvVar{
	Name:        "BUILD_TYPE",
	ViperKey:    BuildType,
	Description: "The build type used to inject parameters based on the environment i.e. stage, prod, dev.",
}

var EnvLogFormat = EnvVar{
	Name:        "LOG_FORMAT",
	ViperKey:    LogFormat,
	Description: "The format to use for logging json or console",
}

var EnvLogLevel = EnvVar{
	Name:        "LOG_LEVEL",
	ViperKey:    LogLevel,
	Description: "The level to log at: debug, info, warn, or error.",
}

var EnvLogSamplingInitial = EnvVar{
	Name:        "LOG_SAMPLING_INITIAL",
	ViperKey:    LogSamplingInitial,
	Description: "The number of the same statements in a second at which sampling should start, if 0 sampling is disabled",
}

var EnvLogSampleThereafter = EnvVar{
	Name:        "LOG_SAMPLE_THEREAFTER",
	ViperKey:    LogSampleThereafter,
	Description: "If log sampling is triggered then this defines that every nth log statement should be logged.",
}

var EnvLogFX = EnvVar{
	Name:        "LOG_FX",
	ViperKey:    LogFX,
	Description: "Indicates if fx logging should occur",
}

var EnvLogHttpRequestLevel = EnvVar{
	Name:        "LOG_HTTP_REQUEST_LEVEL",
	ViperKey:    LogHttpRequestLevel,
	Description: "Indicates the level at which http requests should be logged: ALL, COMPLETION, ERROR, NONE",
}

var EnvLogHttpRequestErrorCodeThreshold = EnvVar{
	Name:        "LOG_HTTP_REQUEST_ERROR_CODE",
	ViperKey:    LogHttpRequestErrorCodeThreshold,
	Description: "If the http request logging is at the ERROR level this indicates the http response code to be considered an error and logged, ie any request with a response code >= to this will be logged.",
}

var EnvLogHttpRequestErrorDurationThreshold = EnvVar{
	Name:        "LOG_HTTP_REQUEST_ERROR_DURATION",
	ViperKey:    LogHttpRequestErrorDurationThreshold,
	Description: "If the http request logging is at the ERROR level this indicates the http response duration to be considered an error and logged, ie any request with a duration >= to this will be logged.\nThe format of this must be a parsable duration ie 5s for 5 seconds.",
}

var EnvListenAddress = EnvVar{
	Name:        "LISTEN_ADDRESS",
	ViperKey:    ListenAddress,
	Description: "The IP address to listen on for incoming http requests, blank means all.",
}

var EnvListenPort = EnvVar{
	Name:        "PORT",
	ViperKey:    ListenPort,
	Description: "The TCP port to listen on for incoming http requests.",
}

var EnvGoogleOAuth2ClientId = EnvVar{
	Name:        "GOOGLE_OAUTH2_CLIENT_ID",
	ViperKey:    GoogleOAuth2ClientId,
	Description: "The client_id to use in the Google OAuth2 flow.",
}

var EnvGoogleOAuth2ClientSecret = EnvVar{
	Name:        "GOOGLE_OAUTH2_CLIENT_SECRET",
	ViperKey:    GoogleOAuth2ClientSecret,
	Description: "The client_secret to use in the Google OAuth2 flow.",
}

var EnvGoogleOAuth2RedirectUrl = EnvVar{
	Name:        "GOOGLE_OAUTH2_REDIRECT_URL",
	ViperKey:    GoogleOAuth2RedirectUrl,
	Description: "The redirect_uri to use in the Google OAuth2 flow.",
}

var EnvRedisAddr = EnvVar{
	Name:        "REDIS_ADDR",
	ViperKey:    RedisAddr,
	Description: "The Redis Connection address that includes the host:port",
}

var EnvRedisPass = EnvVar{
	Name:        "REDIS_PASS",
	ViperKey:    RedisPass,
	Description: "The Redis Connection Password.",
}

var EnvRedisPort = EnvVar{
	Name:        "REDIS_PORT",
	ViperKey:    RedisPort,
	Description: "The redis port.",
}

var EnvIdentityAddress = EnvVar{
	Name:        "IDENTITY_ADDRESS",
	ViperKey:    IdentityAddress,
	Description: "The Identity address that includes the host:port.",
}

var EnvSessionCookieName = EnvVar{
	Name:        "SESSION_COOKIE_NAME",
	ViperKey:    SessionCookieName,
	Description: "The name of the session cookie.",
}

var EnvStateCookieName = EnvVar{
	Name:        "STATE_COOKIE_NAME",
	ViperKey:    StateCookieName,
	Description: "The name of the state cookie.",
}

var EnvSessionDomain = EnvVar{
	Name:        "SESSION_DOMAIN",
	ViperKey:    DomainName,
	Description: "The session domain name.",
}

var EnvOauthScopes = EnvVar{
	Name:        "OAUTH_SCOPES",
	ViperKey:    GoogleOAuth2Scopes,
	Description: "The oauth2 scopes for google tokens.",
}
