package logging

import (
	"code.route.com/platform/iam/tracepoints/internal"
	"context"

	"go.uber.org/zap"

)

// NewRequestLog returns a new internal.RequestLog.
// This method requires that Initialize has been called first.
func NewRequestLog(zap *zap.Logger) internal.RequestLog {
	return &requestLog{
		zapLog{
			logger:  zap,
			sugared: zap.Sugar(),
		},
	}
}

type requestLog struct {
	zapLog
}

func (r *requestLog) ForContext(ctx context.Context) internal.Log {
	l := r.zapLog.logger.With(getContextFields(ctx)...)

	return &zapLog{
		logger:  l,
		sugared: l.Sugar(),
	}
}
