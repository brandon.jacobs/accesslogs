package logging_test

import (
	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal/logging"
	"testing"


	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"

)

func Test_New_Success(t *testing.T) {
	var testCases = []struct {
		LogFormat          string
		LogLevel           string
		SamplingInitial    int
		SamplingThereafter int
		DevMode            bool
	}{
		{"json", "debug", 0, 0, true},
		{"console", "info", 10, 20, false},
		{"console", "warn", 100, 0, true},
		{"console", "error", 100, 75, false},
	}

	for _, tc := range testCases {
		t.Run(tc.LogLevel, func(t *testing.T) {
			cfg := viper.New()
			cfg.Set(config.LogFormat, tc.LogFormat)
			cfg.Set(config.LogLevel, tc.LogLevel)
			cfg.Set(config.LogSamplingInitial, tc.SamplingInitial)
			cfg.Set(config.LogSampleThereafter, tc.SamplingThereafter)
			cfg.Set(config.DevelopmentMode, tc.DevMode)
			zap, err := logging.New(cfg)
			assert.NotNil(t, zap, "returned nil")
			assert.NoError(t, err)
		})
	}
}

func Test_New_Error(t *testing.T) {
	var testCases = []struct {
		LogFormat          string
		LogLevel           string
		SamplingInitial    int
		SamplingThereafter int
		DevMode            bool
	}{
		{"asdf", "debug", 0, 0, true},
		{"console", "asdf", 10, 20, false},
	}

	for _, tc := range testCases {
		t.Run(tc.LogLevel, func(t *testing.T) {
			cfg := viper.New()
			cfg.Set(config.LogFormat, tc.LogFormat)
			cfg.Set(config.LogLevel, tc.LogLevel)
			zap, err := logging.New(cfg)
			assert.Nil(t, zap, "returned not nil")
			assert.Error(t, err)
		})
	}
}
