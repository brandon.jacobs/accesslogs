package logging

import (
	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/spf13/viper"


)

// NewRequestSampler returns a new IncomingHttpRequestSampler
func NewRequestSampler(cfg *viper.Viper) (internal.IncomingHttpRequestSampler, error) {
	level := strings.ToUpper(cfg.GetString(config.LogHttpRequestLevel))
	switch level {
	case "ERROR":
		duration := cfg.GetDuration(config.LogHttpRequestErrorDurationThreshold)
		if duration <= time.Duration(0) {
			return nil, errors.New("http request error duration threshold cannot be <= 0 and must be in a parsable duration format such as 5s for 5 seconds")
		}
		return &errorSampler{
			codeLevel: cfg.GetInt(config.LogHttpRequestErrorCodeThreshold),
			duration:  duration,
		}, nil
	case "COMPLETION":
		return &completionSampler{}, nil
	case "NONE":
		return &noRequestSampler{}, nil
	case "ALL":
		return &allRequestSampler{}, nil
	}

	return nil, fmt.Errorf("unknown http request log level: '%v'", level)
}

type allRequestSampler struct {
}

func (s *allRequestSampler) ShouldLogStart(r *http.Request) bool {
	return true
}

func (s *allRequestSampler) ShouldLogEnd(r *http.Request, responseCode int, duration time.Duration) bool {
	return true
}

type noRequestSampler struct {
}

func (s *noRequestSampler) ShouldLogStart(r *http.Request) bool {
	return false
}

func (s *noRequestSampler) ShouldLogEnd(r *http.Request, responseCode int, duration time.Duration) bool {
	return false
}

type completionSampler struct {
}

func (s *completionSampler) ShouldLogStart(r *http.Request) bool {
	return false
}

func (s *completionSampler) ShouldLogEnd(r *http.Request, responseCode int, duration time.Duration) bool {
	return true
}

type errorSampler struct {
	codeLevel int
	duration  time.Duration
}

func (s *errorSampler) ShouldLogStart(r *http.Request) bool {
	return false
}

func (s *errorSampler) ShouldLogEnd(r *http.Request, responseCode int, duration time.Duration) bool {
	if responseCode >= s.codeLevel {
		return true
	}

	if duration >= s.duration {
		return true
	}

	return false
}
