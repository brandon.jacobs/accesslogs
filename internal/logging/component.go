package logging

import (
	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal"
)

var Component = internal.NewComponent("logging",
	[]config.EnvVar{config.EnvLogLevel, config.EnvLogFormat, config.EnvLogSampleThereafter, config.EnvLogSamplingInitial,
		config.EnvLogFX, config.EnvLogHttpRequestLevel, config.EnvLogHttpRequestErrorDurationThreshold,
		config.EnvLogHttpRequestErrorCodeThreshold},
	NewBackgroundLog, NewRequestLog, NewRequestSampler)
