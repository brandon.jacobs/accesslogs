package logging_test

import (
	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal/logging"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"testing"
	"time"
)

func Test_NewRequestSampler(t *testing.T) {
	var cases = []struct {
		Name            string
		Level           string
		LogStart        bool
		LogEnd          bool
		DurationSeconds int
		Code            int
	}{
		{"None", "none", false, false, 3600, 500},
		{"All", "All", true, true, 3600, 500},
		{"Completion", "completion", false, true, 3600, 500},
		{"ErrorEqualCode", "error", false, true, 1, 404},
		{"ErrorGreaterCode", "error", false, true, 1, 500},
		{"ErrorLessCode", "error", false, false, 1, 403},
		{"ErrorReallyLessCode", "error", false, false, 1, 200},
		{"ErrorEqualDuration", "error", false, true, 5, 200},
		{"ErrorGreaterDuration", "error", false, true, 10, 200},
		{"ErrorLessDuration", "error", false, false, 4, 200},
		{"ErrorReallyLessDuration", "error", false, false, 1, 200},
	}

	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			cfg := viper.New()
			cfg.Set(config.LogHttpRequestLevel, c.Level)
			cfg.Set(config.LogHttpRequestErrorCodeThreshold, 404)
			cfg.Set(config.LogHttpRequestErrorDurationThreshold, "5s")

			sampler, err := logging.NewRequestSampler(cfg)
			if err != nil {
				t.Fatalf("error: %v", err)
			}

			r := httptest.NewRequest("GET", "/", nil)

			if !assert.NotNil(t, sampler, "nil returned") {
				t.FailNow()
			}

			assert.Equal(t, c.LogStart, sampler.ShouldLogStart(r), "LogStart")
			assert.Equal(t, c.LogEnd, sampler.ShouldLogEnd(r, c.Code, time.Second*time.Duration(c.DurationSeconds)), "LogEnd")
		})
	}
}

func Test_NewRequestSampler_BadCode(t *testing.T) {
	cfg := viper.New()
	viper.Set(config.LogHttpRequestLevel, "asdf")
	sampler, err := logging.NewRequestSampler(cfg)

	assert.Error(t, err)
	assert.Nil(t, sampler)
}

func Test_NewRequestSampler_BadDuration(t *testing.T) {
	cfg := viper.New()
	viper.Set(config.LogHttpRequestLevel, "ERROR")
	viper.Set(config.LogHttpRequestErrorDurationThreshold, "asdf")
	sampler, err := logging.NewRequestSampler(cfg)

	assert.Error(t, err)
	assert.Nil(t, sampler)
}
