package logging

import (
	"code.route.com/platform/iam/tracepoints/config"
	"fmt"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"os"
	"sync"
)

var rootLogger *zap.Logger
var rootMutex = sync.Mutex{}

// Initialize will initialize the root logger with the given config. This method must be called before calling any
// of the New* methods to get actual log instances.
func Initialize(cfg *viper.Viper) {
	rootMutex.Lock()
	defer rootMutex.Unlock()

	if rootLogger != nil {
		return
	}

	logger, err := zapConfig(cfg).Build(zap.AddCallerSkip(1),
		zap.Fields(zap.String("version", cfg.GetString(config.Version))))
	if err != nil {
		fmt.Println("Unable to build zap log: ", err)
		os.Exit(1)
	}

	rootLogger = logger
}

func getRootLogger() *zap.Logger {
	rootMutex.Lock()
	defer rootMutex.Unlock()

	if rootLogger == nil {
		panic("logging not initialized")
	}

	return rootLogger
}
