package logging

// FieldError is the structured logging field error information goes in
const FieldError = "error"

// FieldHttpPath is the structured logging field http path information goes in
const FieldHttpPath = "path"

// FieldHttpCode is the structured logging field http status code information goes in
const FieldHttpResponseCode = "httpCode"

// FieldRequestID is the structured logging field request id goes in
const FieldRequestID = "requestID"

// FieldIP is the structured logging field ip goes in
const FieldIP = "ip"

// FieldDurationMS is the structured logging field duration in milliseconds goes in
const FieldDurationMS = "durationMS"

// FieldPrincipalID is the structured logging field for the principal id
const FieldPrincipalID = "principalID"
