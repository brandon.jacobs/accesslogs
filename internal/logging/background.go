package logging

import (
	"code.route.com/platform/iam/tracepoints/internal"
	"go.uber.org/zap"
)

// NewBackgroundLog returns a new internal.BackgroundLog
func NewBackgroundLog(log *zap.Logger) internal.BackgroundLog {
	return &backgroundLog{
		zapLog{
			logger:  log,
			sugared: log.Sugar(),
		},
	}
}

type backgroundLog struct {
	zapLog
}

func (b *backgroundLog) ChildLog(name string) internal.BackgroundLog {
	l := b.zapLog.logger.Named(name)
	return &backgroundLog{
		zapLog{
			logger:  l,
			sugared: l.Sugar(),
		},
	}
}
