package logging

import (
	"fmt"
	"strings"

	"go.uber.org/zap/zapcore"

	"go.uber.org/zap"

	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal"
	"github.com/spf13/viper"
)

func New(cfg *viper.Viper) (*zap.Logger, error) {
	if zc, err := buildZapConfig(cfg); err == nil {
		return zc.Build(zap.AddCallerSkip(1), zap.Fields(zap.String("version", cfg.GetString(config.Version))))
	} else {
		return nil, err
	}
}

func NewStartUp(zap *zap.Logger) internal.StartupLog {
	l := zap.Named("startup")
	return &zapLog{
		logger:  l,
		sugared: l.Sugar(),
	}
}

func buildZapConfig(cfg *viper.Viper) (zap.Config, error) {
	// Determine logger format
	encoding := strings.ToLower(cfg.GetString(config.LogFormat))
	var encoderConfig zapcore.EncoderConfig
	switch encoding {
	case "json":
		encoderConfig = zap.NewProductionEncoderConfig()
	case "console":
		encoderConfig = zap.NewDevelopmentEncoderConfig()
	default:
		return zap.Config{}, fmt.Errorf("unknown log format '%v'\n", encoding)
	}

	var level zap.AtomicLevel
	levelString := strings.ToLower(cfg.GetString(config.LogLevel))
	switch levelString {
	case "debug":
		level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	case "info":
		level = zap.NewAtomicLevelAt(zapcore.InfoLevel)
	case "warn":
		level = zap.NewAtomicLevelAt(zapcore.WarnLevel)
	case "error":
		level = zap.NewAtomicLevelAt(zapcore.ErrorLevel)
	default:
		return zap.Config{}, fmt.Errorf("unknown log level '%v'\n", levelString)
	}

	var sampling *zap.SamplingConfig
	sampleInitial := cfg.GetInt(config.LogSamplingInitial)
	if sampleInitial > 0 {
		sampleThereAfter := cfg.GetInt(config.LogSampleThereafter)
		if sampleThereAfter <= 0 {
			sampleThereAfter = 50
		}

		sampling = &zap.SamplingConfig{
			Initial:    sampleInitial,
			Thereafter: sampleThereAfter,
		}
	}

	return zap.Config{
		Development:      cfg.GetBool(config.DevelopmentMode),
		Encoding:         encoding,
		EncoderConfig:    encoderConfig,
		Level:            level,
		Sampling:         sampling,
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}, nil
}
