package logging

import (
	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal"
	"github.com/spf13/viper"
)

func LogStart(log internal.StartupLog, cfg *viper.Viper) {
	log.Infof("Starting Identity Service Built at %v", cfg.GetString(config.BuildDate))
}
