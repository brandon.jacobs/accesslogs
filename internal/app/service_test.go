package app_test

import (
	"context"
	"errors"
	"sort"
	"sync"
	"testing"
	"time"

	"code.route.com/platform/iam/tracepoints/internal"

	"code.route.com/platform/iam/tracepoints/internal/mocks"

	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal/app"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"go.uber.org/fx"
)

var baseComponent = internal.NewComponent("base", []config.EnvVar{}, newBaseA, newBaseB, newValidatorB)
var commonComponent = internal.NewComponent("common", []config.EnvVar{}, newCommonA, newCommonB, newValidatorA)
var serviceComponent = internal.NewComponent("service", []config.EnvVar{}, newServiceA, newServiceB)
var invalidComponent = internal.NewComponent("invalid", []config.EnvVar{}, newInvalidValidator)

type TestLogger interface {
	LogRun(id int)
	LogArgs(args []string)
}

type testLogger struct {
	Logged []int
	Args   []string
	Lock   sync.Mutex
}

func (l *testLogger) LogRun(id int) {
	l.Lock.Lock()
	defer l.Lock.Unlock()

	l.Logged = append(l.Logged, id)
}

func (l *testLogger) LogArgs(args []string) {
	l.Lock.Lock()
	defer l.Lock.Unlock()
	l.Args = args
}

func (l *testLogger) CopyLog() []int {
	l.Lock.Lock()
	defer l.Lock.Unlock()
	clone := make([]int, len(l.Logged))
	copy(clone, l.Logged)

	return clone
}

func (l *testLogger) CopyArgs() []string {
	l.Lock.Lock()
	defer l.Lock.Unlock()
	clone := make([]string, len(l.Args))
	copy(clone, l.Args)

	return clone
}

func Test_Service_Run(t *testing.T) {
	logger := &testLogger{}

	// Create a root command
	var root app.Command = app.ContainerCommand("root", "root command", "this is a root command")
	root.AddComponent(internal.NewComponent("logger", []config.EnvVar{}, func() TestLogger { return logger }))
	root.AddComponent(baseComponent)

	// Create a service command
	var service app.Command = app.ServiceCommand("test", "test service", "this tests a service command")
	service.AddComponent(commonComponent)
	service.AddComponent(serviceComponent)
	root.AddCommand(service)

	cfg := viper.New()
	cfg.Set(config.LogLevel, "info")
	cfg.Set(config.LogFormat, "console")
	cobra := service.ToCobra(cfg)
	assert.NoError(t, cobra.RunE(cobra, []string{}))

	// Because each thing called logs its int we can check if the slice matches to know everything happened as expected
	// We do sort to ensure the order is correct
	log := logger.CopyLog()
	sort.Ints(log)

	assert.Equal(t, []int{1, 2, 10, 20, 90, 91, 100, 101, 200, 201}, log)
}

func Test_Service_Run_ValidationFailure(t *testing.T) {
	logger := &testLogger{}

	// Create a root command
	var root app.Command = app.ContainerCommand("root", "root command", "this is a root command")
	root.AddComponent(internal.NewComponent("logger", []config.EnvVar{}, func() TestLogger { return logger }))
	root.AddComponent(baseComponent)

	// Create a service command
	var service app.Command = app.ServiceCommand("test", "test service", "this tests a service command")
	service.AddComponent(commonComponent)
	service.AddComponent(invalidComponent)
	service.AddComponent(serviceComponent)
	root.AddCommand(service)

	cfg := viper.New()
	cobra := service.ToCobra(cfg)
	assert.Error(t, cobra.RunE(cobra, []string{}))
}

func Test_Service_Validate_Success(t *testing.T) {
	logger := &testLogger{}

	// Create a root command
	var root app.Command = app.ContainerCommand("root", "root command", "this is a root command")
	root.AddComponent(internal.NewComponent("logger", []config.EnvVar{}, func() TestLogger { return logger }))
	root.AddComponent(baseComponent)

	// Create a service command
	var service app.Command = app.ServiceCommand("test", "test service", "this tests a service command")
	service.AddComponent(commonComponent)
	service.AddComponent(serviceComponent)
	root.AddCommand(service)

	cfg := viper.New()
	assert.NoError(t, service.Validate(cfg))
}

func Test_Service_Validate_MissingDependency(t *testing.T) {
	logger := &testLogger{}

	// Create a root command
	var root app.Command = app.ContainerCommand("root", "root command", "this is a root command")
	root.AddComponent(internal.NewComponent("logger", []config.EnvVar{}, func() TestLogger { return logger }))
	root.AddComponent(baseComponent)

	// Create a service command, missing common component
	var service app.Command = app.ServiceCommand("test", "test service", "this tests a service command")
	service.AddComponent(serviceComponent)
	root.AddCommand(service)

	cfg := viper.New()
	assert.Error(t, service.Validate(cfg))
}

func Test_Service_Validate_NoServices(t *testing.T) {
	logger := &testLogger{}

	// Create a root command
	var root app.Command = app.ContainerCommand("root", "root command", "this is a root command")
	root.AddComponent(internal.NewComponent("logger", []config.EnvVar{}, func() TestLogger { return logger }))
	root.AddComponent(baseComponent)

	// Create a service command
	var service app.Command = app.ServiceCommand("test", "test service", "this tests a service command")
	service.AddComponent(commonComponent)
	// No services added

	root.AddCommand(service)

	cfg := viper.New()
	assert.Error(t, service.Validate(cfg))
}

func Test_ServiceRunner_Start_Error(t *testing.T) {
	log := mocks.NewStartupLog()
	expectedError := errors.New("some error")
	var runner app.Runner = &app.ServiceRunner{
		App: &mockApp{
			StartError: expectedError,
		},
		Log: log,
	}
	logged := false
	log.HandleErr = func(err error, message string) {
		logged = true
		assert.Equal(t, expectedError, err)
		assert.Equal(t, "Error starting app", message)
	}

	err := runner.Run([]string{})
	if assert.Error(t, err) {
		assert.Equal(t, "unable to start app", err.Error())
	}
	assert.True(t, logged, "log entry not made")
}

func Test_ServiceRunner_Stop_Error(t *testing.T) {
	log := mocks.NewStartupLog()
	expectedError := errors.New("some error")
	a := &mockApp{
		StopError: expectedError,
	}
	var runner app.Runner = &app.ServiceRunner{
		App: a,
		Log: log,
	}
	logged := false
	log.HandleErr = func(err error, message string) {
		logged = true
		assert.Equal(t, expectedError, err)
		assert.Equal(t, "Error stopping app", message)
	}

	err := runner.Run([]string{})
	if assert.Error(t, err) {
		assert.Equal(t, "unable to stop app", err.Error())
	}
	assert.True(t, logged, "log entry not made")
	assert.True(t, a.WaitCalled, "Wait not called")
}

type mockApp struct {
	StartError error
	WaitCalled bool
	StopError  error
}

func (m *mockApp) Start(ctx context.Context) error {
	return m.StartError
}

func (m *mockApp) WaitForSignal() {
	m.WaitCalled = true
}

func (m *mockApp) Stop(ctx context.Context) error {
	return m.StopError
}

type serviceA struct {
	A          BaseA
	CA         CommonA
	Log        TestLogger
	Shutdowner fx.Shutdowner
}

type serviceAOut struct {
	fx.Out
	Service app.Service `group:"services"`
}

func newServiceA(a BaseA, ca CommonA, log TestLogger, shutdowner fx.Shutdowner) serviceAOut {
	return serviceAOut{
		Service: &serviceA{
			A:          a,
			CA:         ca,
			Log:        log,
			Shutdowner: shutdowner,
		},
	}
}

func (s *serviceA) Start(ctx context.Context) error {
	go func() {
		s.Log.LogRun(100)
		s.A.A()
		s.CA.CommonA()

		// Sleep then shut us down
		time.Sleep(time.Second * 2)
		if err := s.Shutdowner.Shutdown(); err != nil {
			panic(err)
		}
	}()
	return nil
}

func (s *serviceA) Stop(ctx context.Context) error {
	s.Log.LogRun(101)
	return nil
}

type serviceBOut struct {
	fx.Out
	Service app.Service `group:"services"`
}

type serviceB struct {
	B   BaseB
	CB  CommonB
	Log TestLogger
}

func newServiceB(b BaseB, cb CommonB, log TestLogger) serviceBOut {
	return serviceBOut{
		Service: &serviceB{
			B:   b,
			CB:  cb,
			Log: log,
		},
	}
}

func (s *serviceB) Start(ctx context.Context) error {
	go func() {
		s.Log.LogRun(200)
		s.B.B()
		s.CB.CommonB()
	}()
	return nil
}

func (s *serviceB) Stop(ctx context.Context) error {
	s.Log.LogRun(201)
	return nil
}

type BaseA interface {
	A()
}

type baseA struct {
	Log TestLogger
}

func (b *baseA) A() {
	b.Log.LogRun(1)
}

func newBaseA(log TestLogger) BaseA {
	return &baseA{
		Log: log,
	}
}

type BaseB interface {
	B()
}

type baseB struct {
	Log TestLogger
}

func (b *baseB) B() {
	b.Log.LogRun(2)
}

func newBaseB(log TestLogger) BaseB {
	return &baseB{
		Log: log,
	}
}

type CommonA interface {
	CommonA()
}

type commonA struct {
	Log TestLogger
}

func (b *commonA) CommonA() {
	b.Log.LogRun(10)
}

func newCommonA(log TestLogger) CommonA {
	return &commonA{
		Log: log,
	}
}

type CommonB interface {
	CommonB()
}

type commonB struct {
	Log TestLogger
}

func (b *commonB) CommonB() {
	b.Log.LogRun(20)
}

func newCommonB(log TestLogger) CommonB {
	return &commonB{
		Log: log,
	}
}

type validatorA struct {
	Log TestLogger
}

func (v *validatorA) Validate() error {
	v.Log.LogRun(90)
	return nil
}

type validatorAContainer struct {
	fx.Out
	Validator config.Validator `group:"configValidators"`
}

func newValidatorA(log TestLogger) validatorAContainer {
	return validatorAContainer{
		Validator: &validatorA{
			Log: log,
		},
	}
}

type validatorB struct {
	Log TestLogger
}

func (v *validatorB) Validate() error {
	v.Log.LogRun(91)
	return nil
}

type validatorBContainer struct {
	fx.Out
	Validator config.Validator `group:"configValidators"`
}

func newValidatorB(log TestLogger) validatorBContainer {
	return validatorBContainer{
		Validator: &validatorB{
			Log: log,
		},
	}
}

type invalidValidator struct {
	Log TestLogger
}

func (v *invalidValidator) Validate() error {
	v.Log.LogRun(99)
	return errors.New("testing validation failure")
}

type invalidValidatorContainer struct {
	fx.Out
	Validator config.Validator `group:"configValidators"`
}

func newInvalidValidator(log TestLogger) invalidValidatorContainer {
	return invalidValidatorContainer{
		Validator: &invalidValidator{
			Log: log,
		},
	}
}
