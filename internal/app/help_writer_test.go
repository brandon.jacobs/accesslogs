package app_test

import (
	"strings"
	"testing"

	"code.route.com/platform/iam/tracepoints/config"
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"

	"code.route.com/platform/iam/tracepoints/internal/app"
)

func Test_HelpWriter(t *testing.T) {
	var help app.HelpWriter = app.NewHelpWriter()

	builder := &strings.Builder{}
	cmd := &cobra.Command{Use: "mycmd", Short: "shortdescription", Long: "longdescription", Run: func(cmd *cobra.Command, args []string) {
		// Do nothing, just want this runnable for help generation
	}}
	envVars := []config.EnvVar{
		config.EnvListenAddress,
		config.EnvListenPort,
	}

	assert.NoError(t, help.Write(builder, cmd, envVars))

	// We don't check the full help contents because that would be a pain but check for some key terms
	output := builder.String()
	assert.True(t, strings.Contains(output, "mycmd"), "missing mycmd")
	assert.True(t, strings.Contains(output, "longdescription"), "missing longdescription")
}
