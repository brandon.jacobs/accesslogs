package app_test

import (
	"sort"
	"testing"

	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal"
	"code.route.com/platform/iam/tracepoints/internal/app"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

func Test_Action_Run(t *testing.T) {
	logger := &testLogger{}

	// Create a root command
	var root app.Command = app.ContainerCommand("root", "root command", "this is a root command")
	root.AddComponent(internal.NewComponent("logger", []config.EnvVar{}, func() TestLogger { return logger }))
	root.AddComponent(baseComponent)

	// Create an action command
	var action app.Command = app.ActionCommand("test", "test action", "this tests an action command",
		newAction)
	action.AddComponent(commonComponent)
	root.AddCommand(action)

	cfg := viper.New()
	cfg.Set(config.LogLevel, "info")
	cfg.Set(config.LogFormat, "console")
	cobra := action.ToCobra(cfg)
	expectedArgs := []string{"arga", "argb"}
	assert.NoError(t, cobra.RunE(cobra, expectedArgs))

	// Because each thing called logs its int we can check if the slice matches to know everything happened as expected
	// We do sort to ensure the order is correct
	log := logger.CopyLog()
	sort.Ints(log)

	assert.Equal(t, []int{1, 2, 10, 20, 90, 91, 300}, log)
	assert.Equal(t, expectedArgs, logger.CopyArgs(), "Args not passed through")
}

func Test_Action_ValidationFailure(t *testing.T) {
	logger := &testLogger{}

	// Create a root command
	var root app.Command = app.ContainerCommand("root", "root command", "this is a root command")
	root.AddComponent(internal.NewComponent("logger", []config.EnvVar{}, func() TestLogger { return logger }))
	root.AddComponent(baseComponent)

	// Create an action command
	var action app.Command = app.ActionCommand("test", "test action", "this tests an action command", newAction)
	action.AddComponent(commonComponent)
	action.AddComponent(invalidComponent)
	root.AddCommand(action)

	cfg := viper.New()
	cobra := action.ToCobra(cfg)
	assert.Error(t, cobra.RunE(cobra, []string{}))
}

func Test_Action_Validate_Success(t *testing.T) {
	logger := &testLogger{}

	// Create a root command
	var root app.Command = app.ContainerCommand("root", "root command", "this is a root command")
	root.AddComponent(internal.NewComponent("logger", []config.EnvVar{}, func() TestLogger { return logger }))
	root.AddComponent(baseComponent)

	// Create an action command
	var action app.Command = app.ActionCommand("test", "test action", "this tests an action command", newAction)
	action.AddComponent(commonComponent)
	root.AddCommand(action)

	cfg := viper.New()
	assert.NoError(t, action.Validate(cfg))
}

func Test_Action_Validate_Error(t *testing.T) {
	logger := &testLogger{}

	// Create a root command
	var root app.Command = app.ContainerCommand("root", "root command", "this is a root command")
	root.AddComponent(internal.NewComponent("logger", []config.EnvVar{}, func() TestLogger { return logger }))
	root.AddComponent(baseComponent)

	// Create an action command with no common component
	var action app.Command = app.ActionCommand("test", "test action", "this tests an action command", newAction)
	root.AddCommand(action)

	cfg := viper.New()
	assert.Error(t, action.Validate(cfg))
}

func Test_Action_Validate_NoAction(t *testing.T) {
	logger := &testLogger{}

	// Create a root command
	var root app.Command = app.ContainerCommand("root", "root command", "this is a root command")
	root.AddComponent(internal.NewComponent("logger", []config.EnvVar{}, func() TestLogger { return logger }))
	root.AddComponent(baseComponent)

	// Create an action command with no action component
	var action app.Command = app.ActionCommand("test", "test action", "this tests an action command", func() string { return "hello" })
	action.AddComponent(commonComponent)

	root.AddCommand(action)

	cfg := viper.New()
	assert.Error(t, action.Validate(cfg))
}

func newAction(log TestLogger, a BaseA, ca CommonA, b BaseB, cb CommonB) app.ActionAdapter {
	return &action{
		A:   a,
		CA:  ca,
		B:   b,
		CB:  cb,
		Log: log,
	}
}

type action struct {
	A   BaseA
	CA  CommonA
	B   BaseB
	CB  CommonB
	Log TestLogger
}

func (a *action) Execute(args []string) error {
	a.Log.LogArgs(args)
	a.A.A()
	a.CA.CommonA()
	a.B.B()
	a.CB.CommonB()
	a.Log.LogRun(300)
	return nil
}
