package app

import (
	"io"
	"os"
	"testing"

	"code.route.com/platform/iam/tracepoints/internal"

	"go.uber.org/fx"

	"github.com/spf13/cobra"

	"code.route.com/platform/iam/tracepoints/config"

	"github.com/spf13/viper"

	"github.com/stretchr/testify/assert"
)

// ToCobra -- Basic
// ToCobra -- Leaf -- PreExecute -- All flags & env vars bound
// ToCobra

func newTestCommand(builder Builder, help HelpWriter, envVars ...config.EnvVar) Command {
	return newCommand("test", "stest", "ltest", false, builder, help, envVars)
}

func newContainerCommand(help HelpWriter) Command {
	return newCommand("ctest", "cstest", "cltest", true, nil, help, []config.EnvVar{})
}

func Test_Command_NotContainer(t *testing.T) {
	use := "myuse"
	short := "myshort"
	long := "mylong"

	cmd := newCommand(use, short, long, false, nil, nil, []config.EnvVar{})
	cobra := cmd.ToCobra(viper.New())

	assert.Equal(t, use, cobra.Use, "use")
	assert.Equal(t, short, cobra.Short, "short")
	assert.Equal(t, long, cobra.Long, "long")
	assert.NotNil(t, cobra.PreRunE, "prerun")
	assert.NotNil(t, cobra.RunE, "run")
}

func Test_Command_Container(t *testing.T) {
	use := "myuse"
	short := "myshort"
	long := "mylong"

	cmd := newCommand(use, short, long, true, nil, nil, []config.EnvVar{})
	cobra := cmd.ToCobra(viper.New())

	assert.Equal(t, use, cobra.Use, "use")
	assert.Equal(t, short, cobra.Short, "short")
	assert.Equal(t, long, cobra.Long, "long")
	assert.Nil(t, cobra.PreRunE, "prerun")
}

func Test_Command_PreRunE(t *testing.T) {
	// Set up env vars and note we explicitly have duplicates so we can test we flatten those
	rootCA := internal.NewComponent("rootA", []config.EnvVar{
		config.EnvVar{Name: "TEST_ENV_ROOT_A", ViperKey: "testEnvRootA"},
		config.EnvVar{Name: "TEST_ENV_ROOT_B", ViperKey: "testEnvRootB"},
	})

	rootCB := internal.NewComponent("rootB", []config.EnvVar{
		config.EnvVar{Name: "TEST_ENV_ROOT_C", ViperKey: "testEnvRootC"},
		config.EnvVar{Name: "TEST_ENV_ROOT_A", ViperKey: "testEnvRootA"},
	})

	nodeCA := internal.NewComponent("a", []config.EnvVar{
		config.EnvVar{Name: "TEST_ENV_A", ViperKey: "testEnvA"},
		config.EnvVar{Name: "TEST_ENV_C", ViperKey: "testEnvC"},
	})

	nodeCB := internal.NewComponent("b", []config.EnvVar{
		config.EnvVar{Name: "TEST_ENV_B", ViperKey: "testEnvB"},
		config.EnvVar{Name: "TEST_ENV_C", ViperKey: "testEnvC"},
	})

	extraA := config.EnvVar{Name: "EXTRA_A", ViperKey: "extraA"}
	extraB := config.EnvVar{Name: "EXTRA_B", ViperKey: "extraB"}

	os.Setenv("TEST_ENV_ROOT_A", "roota1")
	os.Setenv("TEST_ENV_ROOT_B", "rootb1")
	os.Setenv("TEST_ENV_ROOT_C", "rootc1")
	os.Setenv("TEST_ENV_A", "a1")
	os.Setenv("TEST_ENV_B", "b1")
	os.Setenv("TEST_ENV_C", "c1")
	os.Setenv("EXTRA_A", "ea")
	os.Setenv("EXTRA_B", "eb")

	var envVars = []struct {
		ViperKey      string
		ExpectedValue string
	}{
		{"testEnvRootA", "roota1"},
		{"testEnvRootB", "rootb1"},
		{"testEnvRootC", "rootc1"},
		{"testEnvA", "a1"},
		{"testEnvB", "b1"},
		{"testEnvC", "c1"},
		{"extraA", "ea"},
		{"extraB", "eb"},
	}

	container := newContainerCommand(nil)
	container.IntFlag("rootIntA", "ia", "iausage")
	container.StringFlag("rootStringA", "sa", "sausage")
	container.BoolFlag("rootBoolA", "rb", "rausage")
	container.DurationFlag("rootDuration", "rd", "rdusage")
	container.AddComponent(rootCA, rootCB)
	// Explicitly add the same component again to show we dedupe
	container.AddComponent(rootCA)

	cmd := newTestCommand(nil, nil, extraA, extraB)
	cmd.IntFlag("int1", "i1", "i1usage")
	cmd.BoolFlag("bool1", "b1", "b1usage")
	cmd.StringFlag("string1", "s1", "s1usage")
	cmd.DurationFlag("duration1", "d1", "d1usage")
	cmd.AddComponent(nodeCA, nodeCB)
	// Explicitly add the same component again to show we dedupe
	cmd.AddComponent(rootCA)

	var flags = []struct {
		ViperKey      string
		ExpectedValue interface{}
	}{
		{"int1", 5},
		{"bool1", true},
		{"string1", "hello"},
		{"duration1", "2s"},
		{"rootIntA", 4},
		{"rootStringA", "test"},
		{"rootBoolA", true},
		{"rootDuration", "3s"},
	}

	container.AddCommand(cmd)

	// Ensure child commands registered
	cfg := viper.New()
	container.ToCobra(cfg)
	cobra := cmd.ToCobra(cfg)
	// Parse flags
	assert.NoError(t, cobra.ParseFlags([]string{"--ia=4", "--sa=test", "--i1=5", "--b1", "--s1=hello", "--d1=2s", "--rb", "--rd=3s"}))

	// Run and see that we have no error
	err := cobra.PreRunE(cobra, []string{})
	assert.NoError(t, err)

	// Check flags bound
	for _, flag := range flags {
		assert.True(t, cfg.IsSet(flag.ViperKey), "flag not set for viperKey %v", flag.ViperKey)
		assert.Equal(t, flag.ExpectedValue, cfg.Get(flag.ViperKey), "value off for viperKey %v", flag.ViperKey)
	}

	// Check Env Vars bound
	for _, envVar := range envVars {
		assert.True(t, cfg.IsSet(envVar.ViperKey), "value not set for viperKey %v", envVar.ViperKey)
		assert.Equal(t, envVar.ExpectedValue, cfg.GetString(envVar.ViperKey), "value off for viperKey %v", envVar.ViperKey)
	}
}

func Test_Command_Help_NotContainer(t *testing.T) {
	// Set up env vars and note we explicitly have duplicates so we can test we flatten those
	rootA := config.EnvVar{Name: "TEST_ENV_ROOT_A", ViperKey: "testEnvRootA"}
	rootB := config.EnvVar{Name: "TEST_ENV_ROOT_B", ViperKey: "testEnvRootB"}
	rootC := config.EnvVar{Name: "TEST_ENV_ROOT_C", ViperKey: "testEnvRootC"}
	rootCA := internal.NewComponent("rootA", []config.EnvVar{rootA, rootB})
	rootCB := internal.NewComponent("rootB", []config.EnvVar{rootC, rootA})
	envA := config.EnvVar{Name: "TEST_ENV_A", ViperKey: "testEnvA"}
	envB := config.EnvVar{Name: "TEST_ENV_B", ViperKey: "testEnvB"}
	envC := config.EnvVar{Name: "TEST_ENV_C", ViperKey: "testEnvC"}
	compA := internal.NewComponent("a", []config.EnvVar{envA, envB})
	compB := internal.NewComponent("b", []config.EnvVar{envA, envC})

	help := &mockHelpWriter{}

	container := newContainerCommand(help)
	container.AddComponent(rootCA, rootCB)
	// Explicitly add the same component again to show we dedupe
	container.AddComponent(rootCA)

	cmd := newTestCommand(nil, help)
	cmd.AddComponent(compA, compB)
	// Explicitly add the same component again to show we dedupe
	cmd.AddComponent(rootCA, compB)
	container.AddCommand(cmd)

	// Ensure child commands registered
	cfg := viper.New()

	container.ToCobra(cfg)
	cobra := cmd.ToCobra(cfg)

	cobra.HelpFunc()(cobra, []string{})
	assert.True(t, help.WriteCalled, "Help writer not called")
	assert.Same(t, cobra, help.Cobra, "Cobra command off")
	assert.Equal(t, []config.EnvVar{envA, envB, envC, rootA, rootB, rootC}, help.EnvVars, "Env vars off")
}

func Test_Command_Help_Container(t *testing.T) {
	// Set up env vars and note we explicitly have duplicates so we can test we flatten those
	rootA := config.EnvVar{Name: "TEST_ENV_ROOT_A", ViperKey: "testEnvRootA"}
	rootB := config.EnvVar{Name: "TEST_ENV_ROOT_B", ViperKey: "testEnvRootB"}
	rootC := config.EnvVar{Name: "TEST_ENV_ROOT_C", ViperKey: "testEnvRootC"}
	rootCA := internal.NewComponent("rootA", []config.EnvVar{rootA, rootB})
	rootCB := internal.NewComponent("rootB", []config.EnvVar{rootC, rootA})

	help := &mockHelpWriter{}
	container := newContainerCommand(help)
	container.AddComponent(rootCA, rootCB)
	// Explicitly add the same component again to show we dedupe
	container.AddComponent(rootCA)

	// Ensure child commands registered
	cfg := viper.New()
	cobra := container.ToCobra(cfg)

	cobra.HelpFunc()(cobra, []string{})
	assert.True(t, help.WriteCalled, "Help writer not called")
	assert.Same(t, cobra, help.Cobra, "Cobra command off")
	assert.Equal(t, []config.EnvVar{rootA, rootB, rootC}, help.EnvVars, "Env vars off")
}

type mockHelpWriter struct {
	WriteCalled bool
	Cobra       *cobra.Command
	EnvVars     []config.EnvVar
}

func (h *mockHelpWriter) Write(w io.Writer, cmd *cobra.Command, envVars []config.EnvVar) error {
	h.WriteCalled = true
	h.Cobra = cmd
	h.EnvVars = envVars

	return nil
}

type mockBuilder struct {
	Config  *viper.Viper
	Options []fx.Option
	Runner  Runner
	Error   error
}

func (m *mockBuilder) BuildRunner(cfg *viper.Viper, options ...fx.Option) (Runner, error) {
	m.Config = cfg
	m.Options = options

	return m.Runner, m.Error
}

func (m *mockBuilder) Validate(cfg *viper.Viper, options ...fx.Option) error {
	return nil
}

type mockRunner struct {
	Called bool
	Error  error
}

func (m *mockRunner) Run(args []string) error {
	m.Called = true
	return m.Error
}
