package app

import (
	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal"
	"code.route.com/platform/iam/tracepoints/internal/mocks"
	"errors"
	"strings"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"go.uber.org/fx"
	"go.uber.org/zap"
)

type depsRunner struct {
	fx.In
	Error   error `optional:"true"`
	Config  *viper.Viper
	Log     *zap.Logger
	StartUp internal.StartupLog
}

func (r *depsRunner) Run(args []string) error {
	return r.Error
}

func Test_Builder_BasicDependencies(t *testing.T) {
	runner := &depsRunner{}

	builder := &builder{
		Runner: runner,
	}
	cfg := viper.New()
	cfg.Set(config.LogFormat, "console")
	cfg.Set(config.LogLevel, "info")
	r, err := builder.BuildRunner(cfg)

	assert.NoError(t, err)
	assert.Same(t, runner, r, "wrong instance returned")
}

func Test_Builder_LogSetupError(t *testing.T) {
	runner := &depsRunner{}

	builder := &builder{
		Runner: runner,
	}
	cfg := viper.New()
	r, err := builder.BuildRunner(cfg)

	if assert.Error(t, err) {
		expectedPrefix := "unable to initialize logging "
		assert.True(t, strings.HasPrefix(err.Error(), expectedPrefix), "expected starts with '%v', got '%v'", expectedPrefix, err.Error())
	}
	assert.Nil(t, r)
}

type fxFailRunner struct {
	fx.In
	Error error `optional:"true"`
	Dep   TestDependency
}

type TestDependency interface{}

func (r *fxFailRunner) Run(args []string) error {
	return r.Error
}

func Test_Builder_FXError(t *testing.T) {
	runner := &fxFailRunner{}

	builder := &builder{
		Runner: runner,
	}
	cfg := viper.New()
	cfg.Set(config.LogFormat, "console")
	cfg.Set(config.LogLevel, "info")
	r, err := builder.BuildRunner(cfg)

	if assert.Error(t, err) {
		expectedPrefix := "unable to initialize fx app"
		assert.True(t, strings.HasPrefix(err.Error(), expectedPrefix), "expected starts with '%v', got '%v'", expectedPrefix, err.Error())
	}
	assert.Nil(t, r)
}

type ValidatorA struct {
	Called bool
	Error  error
}

func (v *ValidatorA) Validate() error {
	v.Called = true
	return v.Error
}

type ValidatorB struct {
	Called bool
	Error  error
}

func (v *ValidatorB) Validate() error {
	v.Called = true
	return v.Error
}

type testValidators struct {
	fx.Out
	ValidatorA config.Validator `group:"configValidators"`
	ValidatorB config.Validator `group:"configValidators"`
}

func Test_Builder_ValidatorsPass(t *testing.T) {
	runner := &depsRunner{}

	builder := &builder{
		Runner: runner,
	}
	cfg := viper.New()
	cfg.Set(config.LogFormat, "console")
	cfg.Set(config.LogLevel, "info")
	a := &ValidatorA{}
	b := &ValidatorB{}
	r, err := builder.BuildRunner(cfg, fx.Provide(func() testValidators {
		return testValidators{
			ValidatorA: a,
			ValidatorB: b,
		}
	}))

	assert.NoError(t, err)
	assert.Same(t, runner, r, "wrong instance returned")
	assert.True(t, a.Called, "A not called")
	assert.True(t, b.Called, "B not called")
}

func Test_Builder_ValidatorsFail(t *testing.T) {
	runner := &depsRunner{}

	log := mocks.NewStartupLog()
	builder := &builder{
		Runner:        runner,
		DefaultOption: fx.Provide(func() internal.StartupLog { return log }),
	}
	cfg := viper.New()
	cfg.Set(config.LogFormat, "console")
	cfg.Set(config.LogLevel, "info")
	a := &ValidatorA{
		Error: errors.New("a error"),
	}
	b := &ValidatorB{
		Error: errors.New("b error"),
	}

	errorALogged := false
	errorBLogged := false
	log.HandleErr = func(err error, message string) {
		if err.Error() == a.Error.Error() {
			errorALogged = true
		} else if err.Error() == b.Error.Error() {
			errorBLogged = true
		} else {
			t.Errorf("unexpected error logged: %v", err)
		}

		assert.Equal(t, "Config validator failed to validate", message)
	}

	r, err := builder.BuildRunner(cfg, fx.Provide(func() testValidators {
		return testValidators{
			ValidatorA: a,
			ValidatorB: b,
		}
	}))

	if assert.Error(t, err) {
		assert.Equal(t, "unable to initialize fx app: one or more config.Validators failed to validate", err.Error())
	}
	assert.Nil(t, r, "nil not returned")
	assert.True(t, a.Called, "A not called")
	assert.True(t, b.Called, "B not called")
	assert.True(t, errorALogged, "A not logged")
	assert.True(t, errorBLogged, "B not logged")
}
