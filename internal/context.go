package internal

import "context"

type ContextKey string

var ContextKeyRequestID ContextKey = "RequestID"
var ContextKeyPrincipalContainer ContextKey = "Principal"

// GetRequestID will return the request id from the context, if not is set then empty string is returned
func GetRequestID(ctx context.Context) string {
	if val, ok := ctx.Value(ContextKeyRequestID).(string); ok {
		return val
	}

	return ""
}

// SetRequestID will create a new context based on the one provided but with the request id set
func SetRequestID(parent context.Context, requestID string) context.Context {
	return context.WithValue(parent, ContextKeyRequestID, requestID)
}

type principalContainer struct {
	principal Principal
}

// GetPrincipal will return the authentication principal from the context if set otherwise nil
func GetPrincipal(ctx context.Context) Principal {
	if val, ok := ctx.Value(ContextKeyPrincipalContainer).(*principalContainer); ok {
		return val.principal
	}

	return nil
}

// GetPrincipalID will return the id of the authenticated principal if set or empty string if none is set
func GetPrincipalID(ctx context.Context) string {
	p := GetPrincipal(ctx)
	if p == nil {
		return ""
	}

	return p.GetID()
}

// PrepContextForPrincipal will create a new context prepared to have the principal set without actually setting
// the principal.  This is to handle the case where you need the principal set further down like in a subsequent middleware
// but the outer caller will need access to the principal as well.  Given contexts are immutable this is a bit of a workaround
func PrepContextForPrincipal(parent context.Context) context.Context {
	return context.WithValue(parent, ContextKeyPrincipalContainer, &principalContainer{})
}

// SetPrincipal will set the principal on the context and return the new context.  If the context has already been
// prepped then it sets it on the container and returns ctx.  This method is not safe to be called concurrently
// from multiple goroutines on the same context but that really should never happen.  If you call it on a context that
// already has a principal set it will panic
func SetPrincipal(parent context.Context, principal Principal) context.Context {
	if val, ok := parent.Value(ContextKeyPrincipalContainer).(*principalContainer); ok {
		if val.principal != nil {
			panic("tried to set principal twice on context")
		}
		val.principal = principal
		return parent
	}

	return context.WithValue(parent, ContextKeyPrincipalContainer, &principalContainer{principal: principal})
}
