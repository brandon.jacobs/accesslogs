package internal

import "time"

type ESClient interface {
	BulkInsertLog(log AccessLog) error
	GetUserLogs(userId string) []AccessLog
	GetAverageAccessTimeByUser(userId string) time.Time
	GetAverageAccessTimeAllUsers() []time.Time

}
