package internal

// Principal is the interface exposed by security principals
type Principal interface {
	// GetID gets the ID of the principal
	GetID() string
}
