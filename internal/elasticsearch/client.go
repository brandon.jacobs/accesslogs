package elasticsearch
import (
	"code.route.com/platform/iam/tracepoints/internal"
	"context"
	"encoding/json"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esutil"
	"strings"
	"time"
)
type elasticClient struct {
	client *elasticsearch.Client
	bulkIndexer esutil.BulkIndexer
	log internal.BackgroundLog
}

type location struct {
	lat string `json:"lat"`
	lon string `json:"long"`
}

type record struct {
	location location `json:"location"`
	userId string `json:"user_id"`
	sessionId string `json:"session_id"`
	loggedAtUtc time.Time `json:"logged_at_utc"`
}

func (e elasticClient) BulkInsertLog(log internal.AccessLog) error {
	rec := map[string]interface{}{
		"user_id" : log.UserId,
		"action": "login",
		"location": map[string]interface{}{
			"lat": log.Latitude,
			"lon": log.Longitude,
		},
		"session_id": log.SessionId,
		"logged_at_utc": log.Time,
	}
	input, _ := json.Marshal(rec)
	e.log.InfofCtx(context.Background(),"inserting rec: %s", string(input))

	err := e.bulkIndexer.Add(context.Background(),esutil.BulkIndexerItem{
		Index:           "access_logs",
		Action:          "index",
		Body:            strings.NewReader(string(input)),
	})
	if err != nil {
		e.log.Errf(err,"error inserting:%s",err.Error())
	}
	return err
}

func (e elasticClient) GetUserLogs(userId string) []internal.AccessLog {
	panic("implement me")
}

func (e elasticClient) GetAverageAccessTimeByUser(userId string) time.Time {
	panic("implement me")
}

func (e elasticClient) GetAverageAccessTimeAllUsers() []time.Time {
	panic("implement me")
}

func NewESClient(log internal.BackgroundLog) internal.ESClient {
	cfg := elasticsearch.Config{
		Addresses:             []string{"https://search-identity-access-test-pdzjjgstsbkgwt4a3je4sup6a4.us-east-1.es.amazonaws.com"},
		Username:              "xxx",
		Password:              "xxx",
	}
	client, err  := elasticsearch.NewClient(cfg)
	if err != nil {
		log.Errf(err,"error creating client")
	}
	bulkIndexer, _ := esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
		FlushInterval:       100,
		Client:              client,
		Index:               "access_logs",

	})
	if err != nil {
		return nil 
	}
	return &elasticClient{client: client,bulkIndexer: bulkIndexer, log: log}
}
