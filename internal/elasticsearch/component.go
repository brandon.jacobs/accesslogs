package elasticsearch

import (
	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal"
)

var Component = internal.NewComponent("elasticsearch",[]config.EnvVar{},NewESClient)
