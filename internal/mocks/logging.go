package mocks

import (
	"context"
	"net/http"
	"time"

	"code.route.com/platform/iam/tracepoints/internal"
)

// RequestLog is a mock internal.RequestLog instance
type RequestLog struct {
	HandleForContext func(ctx context.Context) internal.Log
	HandleDebugCtx   func(ctx context.Context, args ...interface{})
	HandleDebugfCtx  func(ctx context.Context, template string, args ...interface{})
	HandleDebugwCtx  func(ctx context.Context, message string, keyValues ...interface{})
	HandleInfoCtx    func(ctx context.Context, args ...interface{})
	HandleInfofCtx   func(ctx context.Context, template string, args ...interface{})
	HandleInfowCtx   func(ctx context.Context, message string, keyValues ...interface{})
	HandleWarnCtx    func(ctx context.Context, args ...interface{})
	HandleWarnfCtx   func(ctx context.Context, template string, args ...interface{})
	HandleWarnwCtx   func(ctx context.Context, message string, keyValues ...interface{})
	HandleErrorCtx   func(ctx context.Context, args ...interface{})
	HandleErrorfCtx  func(ctx context.Context, template string, args ...interface{})
	HandleErrorwCtx  func(ctx context.Context, message string, keyValues ...interface{})
	HandleErrCtx     func(ctx context.Context, err error, message string)
	HandleErrfCtx    func(ctx context.Context, err error, template string, args ...interface{})
	ErrfCtxCalled    bool
	HandleErrwCtx    func(ctx context.Context, err error, message string, keyValues ...interface{})
}

// NewRequestLog creates a new mock internal.RequestLog instance
func NewRequestLog() *RequestLog {
	return &RequestLog{}
}

// Debugf will call HandleForContext if set otherwise returns nil
func (l *RequestLog) ForContext(ctx context.Context) internal.Log {
	if l.HandleForContext != nil {
		return l.HandleForContext(ctx)
	}

	return nil
}

// DebugCtx will call HandleDebugCtx if set
func (l *RequestLog) DebugCtx(ctx context.Context, message string) {
	if l.HandleDebugCtx != nil {
		l.HandleDebugCtx(ctx, message)
	}
}

// DebugfCtx will call HandleDebugfCtx if set
func (l *RequestLog) DebugfCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleDebugfCtx != nil {
		l.HandleDebugfCtx(ctx, template, args...)
	}
}

// DebugwCtx will call HandleDebugwCtx if set
func (l *RequestLog) DebugwCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleDebugwCtx != nil {
		l.HandleDebugwCtx(ctx, message, keyValues...)
	}
}

// InfoCtx will call HandleInfoCtx if set
func (l *RequestLog) InfoCtx(ctx context.Context, message string) {
	if l.HandleInfoCtx != nil {
		l.HandleInfoCtx(ctx, message)
	}
}

// InfofCtx will call HandleInfofCtx if set
func (l *RequestLog) InfofCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleInfofCtx != nil {
		l.HandleInfofCtx(ctx, template, args...)
	}
}

// InfowCtx will call HandleInfowCtx if set
func (l *RequestLog) InfowCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleInfowCtx != nil {
		l.HandleInfowCtx(ctx, message, keyValues...)
	}
}

// WarnCtx will call HandleWarnCtx if set
func (l *RequestLog) WarnCtx(ctx context.Context, message string) {
	if l.HandleWarnCtx != nil {
		l.HandleWarnCtx(ctx, message)
	}
}

// WarnfCtx will call HandleWarnfCtx if set
func (l *RequestLog) WarnfCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleWarnfCtx != nil {
		l.HandleWarnfCtx(ctx, template, args...)
	}
}

// WarnwCtx will call HandleWarnwCtx if set
func (l *RequestLog) WarnwCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleWarnwCtx != nil {
		l.HandleWarnwCtx(ctx, message, keyValues...)
	}
}

// ErrorCtx will call HandleErrorCtx if set
func (l *RequestLog) ErrorCtx(ctx context.Context, message string) {
	if l.HandleErrorCtx != nil {
		l.HandleErrorCtx(ctx, message)
	}
}

// ErrorfCtx will call HandleErrorfCtx if set
func (l *RequestLog) ErrorfCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleErrorfCtx != nil {
		l.HandleErrorfCtx(ctx, template, args...)
	}
}

// ErrorwCtx will call HandleErrorwCtx if set
func (l *RequestLog) ErrorwCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleErrorwCtx != nil {
		l.HandleErrorwCtx(ctx, message, keyValues...)
	}
}

// ErrCtx will call HandleErrCtx if set
func (l *RequestLog) ErrCtx(ctx context.Context, err error, message string) {
	if l.HandleErrCtx != nil {
		l.HandleErrCtx(ctx, err, message)
	}
}

// ErrfCtx will call HandleErrfCtx if set
func (l *RequestLog) ErrfCtx(ctx context.Context, err error, template string, args ...interface{}) {
	l.ErrfCtxCalled = true
	if l.HandleErrfCtx != nil {
		l.HandleErrfCtx(ctx, err, template, args...)
	}
}

// ErrwCtx will call HandleErrwCtx if set
func (l *RequestLog) ErrwCtx(ctx context.Context, err error, message string, keyValues ...interface{}) {
	if l.HandleErrwCtx != nil {
		l.HandleErrwCtx(ctx, err, message, keyValues...)
	}
}

// log is a mock internal.log implementation
type Log struct {
	HandleDebug  func(args ...interface{})
	HandleDebugf func(template string, args ...interface{})
	HandleDebugw func(message string, keyValues ...interface{})
	HandleInfo   func(args ...interface{})
	HandleInfof  func(template string, args ...interface{})
	HandleInfow  func(message string, keyValues ...interface{})
	HandleWarn   func(args ...interface{})
	HandleWarnf  func(template string, args ...interface{})
	HandleWarnw  func(message string, keyValues ...interface{})
	HandleError  func(args ...interface{})
	HandleErrorf func(template string, args ...interface{})
	HandleErrorw func(message string, keyValues ...interface{})
	HandleErr    func(err error, message string)
	HandleErrf   func(err error, template string, args ...interface{})
	HandleErrw   func(err error, message string, keyValues ...interface{})
}

// Returns a new mock log implementation
func NewLog() *Log {
	return &Log{}
}

// Debug will call HandleDebug if set
func (l *Log) Debug(args ...interface{}) {
	if l.HandleDebug != nil {
		l.HandleDebug(args...)
	}
}

// Debugf will call HandleDebugf if set
func (l *Log) Debugf(template string, args ...interface{}) {
	if l.HandleDebugf != nil {
		l.HandleDebugf(template, args...)
	}
}

// Debugw will call HandleDebugw if set
func (l *Log) Debugw(message string, keyValues ...interface{}) {
	if l.HandleDebugw != nil {
		l.HandleDebugw(message, keyValues...)
	}
}

// Info will call HandleInfo if set
func (l *Log) Info(args ...interface{}) {
	if l.HandleInfo != nil {
		l.HandleInfo(args...)
	}
}

// Infof will call HandleInfof if set
func (l *Log) Infof(template string, args ...interface{}) {
	if l.HandleInfof != nil {
		l.HandleInfof(template, args...)
	}
}

// Infow will call HandleInfow if set
func (l *Log) Infow(message string, keyValues ...interface{}) {
	if l.HandleInfow != nil {
		l.HandleInfow(message, keyValues...)
	}
}

// Warn will call HandleWarn if set
func (l *Log) Warn(args ...interface{}) {
	if l.HandleWarn != nil {
		l.HandleWarn(args...)
	}
}

// Warnf will call HandleWarnf if set
func (l *Log) Warnf(template string, args ...interface{}) {
	if l.HandleWarnf != nil {
		l.HandleWarnf(template, args...)
	}
}

// Warnw will call HandleWarnw if set
func (l *Log) Warnw(message string, keyValues ...interface{}) {
	if l.HandleWarnw != nil {
		l.HandleWarnw(message, keyValues...)
	}
}

// Error will call HandleError if set
func (l *Log) Error(args ...interface{}) {
	if l.HandleError != nil {
		l.HandleError(args...)
	}
}

// Errorf will call HandleErrorf if set
func (l *Log) Errorf(template string, args ...interface{}) {
	if l.HandleErrorf != nil {
		l.HandleErrorf(template, args...)
	}
}

// Errorw will call HandleErrorw if set
func (l *Log) Errorw(message string, keyValues ...interface{}) {
	if l.HandleErrorw != nil {
		l.HandleErrorw(message, keyValues...)
	}
}

// Err will call HandleErr if set
func (l *Log) Err(err error, message string) {
	if l.HandleErr != nil {
		l.HandleErr(err, message)
	}
}

// Errf will call HandleErrf if set
func (l *Log) Errf(err error, template string, args ...interface{}) {
	if l.HandleErrf != nil {
		l.HandleErrf(err, template, args...)
	}
}

// Errw will call HandleErrw if set
func (l *Log) Errw(err error, message string, keyValues ...interface{}) {
	if l.HandleErrw != nil {
		l.HandleErrw(err, message, keyValues...)
	}
}

// BackgroundLog is a mock internal.BackgroundLog implementation
type BackgroundLog struct {
	HandleChildLog  func(name string) internal.BackgroundLog
	HandleDebug     func(args ...interface{})
	HandleDebugf    func(template string, args ...interface{})
	HandleDebugw    func(message string, keyValues ...interface{})
	HandleInfo      func(args ...interface{})
	HandleInfof     func(template string, args ...interface{})
	HandleInfow     func(message string, keyValues ...interface{})
	HandleWarn      func(args ...interface{})
	HandleWarnf     func(template string, args ...interface{})
	HandleWarnw     func(message string, keyValues ...interface{})
	HandleError     func(args ...interface{})
	HandleErrorf    func(template string, args ...interface{})
	HandleErrorw    func(message string, keyValues ...interface{})
	HandleErr       func(err error, message string)
	HandleErrf      func(err error, template string, args ...interface{})
	HandleErrw      func(err error, message string, keyValues ...interface{})
	HandleDebugCtx  func(ctx context.Context, args ...interface{})
	HandleDebugfCtx func(ctx context.Context, template string, args ...interface{})
	HandleDebugwCtx func(ctx context.Context, message string, keyValues ...interface{})
	HandleInfoCtx   func(ctx context.Context, args ...interface{})
	HandleInfofCtx  func(ctx context.Context, template string, args ...interface{})
	HandleInfowCtx  func(ctx context.Context, message string, keyValues ...interface{})
	HandleWarnCtx   func(ctx context.Context, args ...interface{})
	HandleWarnfCtx  func(ctx context.Context, template string, args ...interface{})
	HandleWarnwCtx  func(ctx context.Context, message string, keyValues ...interface{})
	HandleErrorCtx  func(ctx context.Context, args ...interface{})
	HandleErrorfCtx func(ctx context.Context, template string, args ...interface{})
	HandleErrorwCtx func(ctx context.Context, message string, keyValues ...interface{})
	HandleErrCtx    func(ctx context.Context, err error, message string)
	HandleErrfCtx   func(ctx context.Context, err error, template string, args ...interface{})
	HandleErrwCtx   func(ctx context.Context, err error, message string, keyValues ...interface{})
}

// NewBackgroundLog returns a new mock internal.BackgroundLog implementation
func NewBackgroundLog() *BackgroundLog {
	return &BackgroundLog{}
}

// ChildLog will call HandleChildLog if set else it returns nil
func (l *BackgroundLog) ChildLog(name string) internal.BackgroundLog {
	if l.HandleChildLog != nil {
		return l.HandleChildLog(name)
	}

	return nil
}

// Debug will call HandleDebug if set
func (l *BackgroundLog) Debug(args ...interface{}) {
	if l.HandleDebug != nil {
		l.HandleDebug(args...)
	}
}

// Debugf will call HandleDebugf if set
func (l *BackgroundLog) Debugf(template string, args ...interface{}) {
	if l.HandleDebugf != nil {
		l.HandleDebugf(template, args...)
	}
}

// Debugw will call HandleDebugw if set
func (l *BackgroundLog) Debugw(message string, keyValues ...interface{}) {
	if l.HandleDebugw != nil {
		l.HandleDebugw(message, keyValues...)
	}
}

// Info will call HandleInfo if set
func (l *BackgroundLog) Info(args ...interface{}) {
	if l.HandleInfo != nil {
		l.HandleInfo(args...)
	}
}

// Infof will call HandleInfof if set
func (l *BackgroundLog) Infof(template string, args ...interface{}) {
	if l.HandleInfof != nil {
		l.HandleInfof(template, args...)
	}
}

// Infow will call HandleInfow if set
func (l *BackgroundLog) Infow(message string, keyValues ...interface{}) {
	if l.HandleInfow != nil {
		l.HandleInfow(message, keyValues...)
	}
}

// Warn will call HandleWarn if set
func (l *BackgroundLog) Warn(args ...interface{}) {
	if l.HandleWarn != nil {
		l.HandleWarn(args...)
	}
}

// Warnf will call HandleWarnf if set
func (l *BackgroundLog) Warnf(template string, args ...interface{}) {
	if l.HandleWarnf != nil {
		l.HandleWarnf(template, args...)
	}
}

// Warnw will call HandleWarnw if set
func (l *BackgroundLog) Warnw(message string, keyValues ...interface{}) {
	if l.HandleWarnw != nil {
		l.HandleWarnw(message, keyValues...)
	}
}

// Error will call HandleError if set
func (l *BackgroundLog) Error(args ...interface{}) {
	if l.HandleError != nil {
		l.HandleError(args...)
	}
}

// Errorf will call HandleErrorf if set
func (l *BackgroundLog) Errorf(template string, args ...interface{}) {
	if l.HandleErrorf != nil {
		l.HandleErrorf(template, args...)
	}
}

// Errorw will call HandleErrorw if set
func (l *BackgroundLog) Errorw(message string, keyValues ...interface{}) {
	if l.HandleErrorw != nil {
		l.HandleErrorw(message, keyValues...)
	}
}

// Err will call HandleErr if set
func (l *BackgroundLog) Err(err error, message string) {
	if l.HandleErr != nil {
		l.HandleErr(err, message)
	}
}

// Errf will call HandleErrf if set
func (l *BackgroundLog) Errf(err error, template string, args ...interface{}) {
	if l.HandleErrf != nil {
		l.HandleErrf(err, template, args...)
	}
}

// Errw will call HandleErrw if set
func (l *BackgroundLog) Errw(err error, message string, keyValues ...interface{}) {
	if l.HandleErrw != nil {
		l.HandleErrw(err, message, keyValues...)
	}
}

// DebugCtx will call HandleDebugCtx if set
func (l *BackgroundLog) DebugCtx(ctx context.Context, message string) {
	if l.HandleDebugCtx != nil {
		l.HandleDebugCtx(ctx, message)
	}
}

// DebugfCtx will call HandleDebugfCtx if set
func (l *BackgroundLog) DebugfCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleDebugfCtx != nil {
		l.HandleDebugfCtx(ctx, template, args...)
	}
}

// DebugwCtx will call HandleDebugwCtx if set
func (l *BackgroundLog) DebugwCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleDebugwCtx != nil {
		l.HandleDebugwCtx(ctx, message, keyValues...)
	}
}

// InfoCtx will call HandleInfoCtx if set
func (l *BackgroundLog) InfoCtx(ctx context.Context, message string) {
	if l.HandleInfoCtx != nil {
		l.HandleInfoCtx(ctx, message)
	}
}

// InfofCtx will call HandleInfofCtx if set
func (l *BackgroundLog) InfofCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleInfofCtx != nil {
		l.HandleInfofCtx(ctx, template, args...)
	}
}

// InfowCtx will call HandleInfowCtx if set
func (l *BackgroundLog) InfowCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleInfowCtx != nil {
		l.HandleInfowCtx(ctx, message, keyValues...)
	}
}

// WarnCtx will call HandleWarnCtx if set
func (l *BackgroundLog) WarnCtx(ctx context.Context, message string) {
	if l.HandleWarnCtx != nil {
		l.HandleWarnCtx(ctx, message)
	}
}

// WarnfCtx will call HandleWarnfCtx if set
func (l *BackgroundLog) WarnfCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleWarnfCtx != nil {
		l.HandleWarnfCtx(ctx, template, args...)
	}
}

// WarnwCtx will call HandleWarnwCtx if set
func (l *BackgroundLog) WarnwCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleWarnwCtx != nil {
		l.HandleWarnwCtx(ctx, message, keyValues...)
	}
}

// ErrorCtx will call HandleErrorCtx if set
func (l *BackgroundLog) ErrorCtx(ctx context.Context, message string) {
	if l.HandleErrorCtx != nil {
		l.HandleErrorCtx(ctx, message)
	}
}

// ErrorfCtx will call HandleErrorfCtx if set
func (l *BackgroundLog) ErrorfCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleErrorfCtx != nil {
		l.HandleErrorfCtx(ctx, template, args...)
	}
}

// ErrorwCtx will call HandleErrorwCtx if set
func (l *BackgroundLog) ErrorwCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleErrorwCtx != nil {
		l.HandleErrorwCtx(ctx, message, keyValues...)
	}
}

// ErrCtx will call HandleErrCtx if set
func (l *BackgroundLog) ErrCtx(ctx context.Context, err error, message string) {
	if l.HandleErrCtx != nil {
		l.HandleErrCtx(ctx, err, message)
	}
}

// ErrfCtx will call HandleErrfCtx if set
func (l *BackgroundLog) ErrfCtx(ctx context.Context, err error, template string, args ...interface{}) {
	if l.HandleErrfCtx != nil {
		l.HandleErrfCtx(ctx, err, template, args...)
	}
}

// ErrwCtx will call HandleErrwCtx if set
func (l *BackgroundLog) ErrwCtx(ctx context.Context, err error, message string, keyValues ...interface{}) {
	if l.HandleErrwCtx != nil {
		l.HandleErrwCtx(ctx, err, message, keyValues...)
	}
}

// StartupLog is a mock internal.StartupLog implementation
type StartupLog struct {
	HandleDebug     func(args ...interface{})
	HandleDebugf    func(template string, args ...interface{})
	HandleDebugw    func(message string, keyValues ...interface{})
	HandleInfo      func(args ...interface{})
	HandleInfof     func(template string, args ...interface{})
	HandleInfow     func(message string, keyValues ...interface{})
	HandleWarn      func(args ...interface{})
	HandleWarnf     func(template string, args ...interface{})
	HandleWarnw     func(message string, keyValues ...interface{})
	HandleError     func(args ...interface{})
	HandleErrorf    func(template string, args ...interface{})
	HandleErrorw    func(message string, keyValues ...interface{})
	HandleErr       func(err error, message string)
	HandleErrf      func(err error, template string, args ...interface{})
	HandleErrw      func(err error, message string, keyValues ...interface{})
	HandleDebugCtx  func(ctx context.Context, args ...interface{})
	HandleDebugfCtx func(ctx context.Context, template string, args ...interface{})
	HandleDebugwCtx func(ctx context.Context, message string, keyValues ...interface{})
	HandleInfoCtx   func(ctx context.Context, args ...interface{})
	HandleInfofCtx  func(ctx context.Context, template string, args ...interface{})
	HandleInfowCtx  func(ctx context.Context, message string, keyValues ...interface{})
	HandleWarnCtx   func(ctx context.Context, args ...interface{})
	HandleWarnfCtx  func(ctx context.Context, template string, args ...interface{})
	HandleWarnwCtx  func(ctx context.Context, message string, keyValues ...interface{})
	HandleErrorCtx  func(ctx context.Context, args ...interface{})
	HandleErrorfCtx func(ctx context.Context, template string, args ...interface{})
	HandleErrorwCtx func(ctx context.Context, message string, keyValues ...interface{})
	HandleErrCtx    func(ctx context.Context, err error, message string)
	HandleErrfCtx   func(ctx context.Context, err error, template string, args ...interface{})
	HandleErrwCtx   func(ctx context.Context, err error, message string, keyValues ...interface{})
}

// NewBackgroundLog returns a new mock internal.BackgroundLog implementation
func NewStartupLog() *StartupLog {
	return &StartupLog{}
}

// Debug will call HandleDebug if set
func (l *StartupLog) Debug(args ...interface{}) {
	if l.HandleDebug != nil {
		l.HandleDebug(args...)
	}
}

// Debugf will call HandleDebugf if set
func (l *StartupLog) Debugf(template string, args ...interface{}) {
	if l.HandleDebugf != nil {
		l.HandleDebugf(template, args...)
	}
}

// Debugw will call HandleDebugw if set
func (l *StartupLog) Debugw(message string, keyValues ...interface{}) {
	if l.HandleDebugw != nil {
		l.HandleDebugw(message, keyValues...)
	}
}

// Info will call HandleInfo if set
func (l *StartupLog) Info(args ...interface{}) {
	if l.HandleInfo != nil {
		l.HandleInfo(args...)
	}
}

// Infof will call HandleInfof if set
func (l *StartupLog) Infof(template string, args ...interface{}) {
	if l.HandleInfof != nil {
		l.HandleInfof(template, args...)
	}
}

// Infow will call HandleInfow if set
func (l *StartupLog) Infow(message string, keyValues ...interface{}) {
	if l.HandleInfow != nil {
		l.HandleInfow(message, keyValues...)
	}
}

// Warn will call HandleWarn if set
func (l *StartupLog) Warn(args ...interface{}) {
	if l.HandleWarn != nil {
		l.HandleWarn(args...)
	}
}

// Warnf will call HandleWarnf if set
func (l *StartupLog) Warnf(template string, args ...interface{}) {
	if l.HandleWarnf != nil {
		l.HandleWarnf(template, args...)
	}
}

// Warnw will call HandleWarnw if set
func (l *StartupLog) Warnw(message string, keyValues ...interface{}) {
	if l.HandleWarnw != nil {
		l.HandleWarnw(message, keyValues...)
	}
}

// Error will call HandleError if set
func (l *StartupLog) Error(args ...interface{}) {
	if l.HandleError != nil {
		l.HandleError(args...)
	}
}

// Errorf will call HandleErrorf if set
func (l *StartupLog) Errorf(template string, args ...interface{}) {
	if l.HandleErrorf != nil {
		l.HandleErrorf(template, args...)
	}
}

// Errorw will call HandleErrorw if set
func (l *StartupLog) Errorw(message string, keyValues ...interface{}) {
	if l.HandleErrorw != nil {
		l.HandleErrorw(message, keyValues...)
	}
}

// Err will call HandleErr if set
func (l *StartupLog) Err(err error, message string) {
	if l.HandleErr != nil {
		l.HandleErr(err, message)
	}
}

// Errf will call HandleErrf if set
func (l *StartupLog) Errf(err error, template string, args ...interface{}) {
	if l.HandleErrf != nil {
		l.HandleErrf(err, template, args...)
	}
}

// Errw will call HandleErrw if set
func (l *StartupLog) Errw(err error, message string, keyValues ...interface{}) {
	if l.HandleErrw != nil {
		l.HandleErrw(err, message, keyValues...)
	}
}

// DebugCtx will call HandleDebugCtx if set
func (l *StartupLog) DebugCtx(ctx context.Context, message string) {
	if l.HandleDebugCtx != nil {
		l.HandleDebugCtx(ctx, message)
	}
}

// DebugfCtx will call HandleDebugfCtx if set
func (l *StartupLog) DebugfCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleDebugfCtx != nil {
		l.HandleDebugfCtx(ctx, template, args...)
	}
}

// DebugwCtx will call HandleDebugwCtx if set
func (l *StartupLog) DebugwCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleDebugwCtx != nil {
		l.HandleDebugwCtx(ctx, message, keyValues...)
	}
}

// InfoCtx will call HandleInfoCtx if set
func (l *StartupLog) InfoCtx(ctx context.Context, message string) {
	if l.HandleInfoCtx != nil {
		l.HandleInfoCtx(ctx, message)
	}
}

// InfofCtx will call HandleInfofCtx if set
func (l *StartupLog) InfofCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleInfofCtx != nil {
		l.HandleInfofCtx(ctx, template, args...)
	}
}

// InfowCtx will call HandleInfowCtx if set
func (l *StartupLog) InfowCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleInfowCtx != nil {
		l.HandleInfowCtx(ctx, message, keyValues...)
	}
}

// WarnCtx will call HandleWarnCtx if set
func (l *StartupLog) WarnCtx(ctx context.Context, message string) {
	if l.HandleWarnCtx != nil {
		l.HandleWarnCtx(ctx, message)
	}
}

// WarnfCtx will call HandleWarnfCtx if set
func (l *StartupLog) WarnfCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleWarnfCtx != nil {
		l.HandleWarnfCtx(ctx, template, args...)
	}
}

// WarnwCtx will call HandleWarnwCtx if set
func (l *StartupLog) WarnwCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleWarnwCtx != nil {
		l.HandleWarnwCtx(ctx, message, keyValues...)
	}
}

// ErrorCtx will call HandleErrorCtx if set
func (l *StartupLog) ErrorCtx(ctx context.Context, message string) {
	if l.HandleErrorCtx != nil {
		l.HandleErrorCtx(ctx, message)
	}
}

// ErrorfCtx will call HandleErrorfCtx if set
func (l *StartupLog) ErrorfCtx(ctx context.Context, template string, args ...interface{}) {
	if l.HandleErrorfCtx != nil {
		l.HandleErrorfCtx(ctx, template, args...)
	}
}

// ErrorwCtx will call HandleErrorwCtx if set
func (l *StartupLog) ErrorwCtx(ctx context.Context, message string, keyValues ...interface{}) {
	if l.HandleErrorwCtx != nil {
		l.HandleErrorwCtx(ctx, message, keyValues...)
	}
}

// ErrCtx will call HandleErrCtx if set
func (l *StartupLog) ErrCtx(ctx context.Context, err error, message string) {
	if l.HandleErrCtx != nil {
		l.HandleErrCtx(ctx, err, message)
	}
}

// ErrfCtx will call HandleErrfCtx if set
func (l *StartupLog) ErrfCtx(ctx context.Context, err error, template string, args ...interface{}) {
	if l.HandleErrfCtx != nil {
		l.HandleErrfCtx(ctx, err, template, args...)
	}
}

// ErrwCtx will call HandleErrwCtx if set
func (l *StartupLog) ErrwCtx(ctx context.Context, err error, message string, keyValues ...interface{}) {
	if l.HandleErrwCtx != nil {
		l.HandleErrwCtx(ctx, err, message, keyValues...)
	}
}

// IncomingHttpRequestSampler is a mock implementation of the internal.IncomingHttpRequestSampler interface
type IncomingHttpRequestSampler struct {
	HandleShouldLogStart func(r *http.Request) bool
	HandleShouldLogEnd   func(r *http.Request, responseCode int, duration time.Duration) bool
}

// NewIncomingHttpRequestSampler returns a mock implementation of the internal.IncomingHttpRequestSampler interface
func NewIncomingHttpRequestSampler() *IncomingHttpRequestSampler {
	return &IncomingHttpRequestSampler{}
}

// ShouldLogStart will call HandleShouldLogStart if set otherwise it just returns false
func (m *IncomingHttpRequestSampler) ShouldLogStart(r *http.Request) bool {
	if m.HandleShouldLogStart != nil {
		return m.HandleShouldLogStart(r)
	}

	return false
}

// ShouldLogEnd will call HandleShouldLogEnd if set otherwise it just returns false
func (m *IncomingHttpRequestSampler) ShouldLogEnd(r *http.Request, responseCode int, duration time.Duration) bool {
	if m.HandleShouldLogEnd != nil {
		return m.HandleShouldLogEnd(r, responseCode, duration)
	}

	return false
}
