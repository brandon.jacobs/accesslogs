package server

import (
	"code.route.com/platform/iam/tracepoints/internal"
	"code.route.com/platform/iam/tracepoints/internal/app"
	"code.route.com/platform/iam/tracepoints/internal/generator"
	"context"
	"go.uber.org/fx"
)

type serverOut struct {
	fx.Out
	Service app.Service `group:"services"`
}

func newServer(client internal.ESClient) serverOut {
	return serverOut{
		Service: &messageGenerator{
			messageCount: 1000,
			esClient:     client,
		},
	}
}

type messageGenerator struct {
	esClient     internal.ESClient
	messageCount int
}

func (svr *messageGenerator) Start(ctx context.Context) error {
	for i := 0; i < svr.messageCount; i++ {
		log := generator.GenerateRandomAccessLog()
		_ = svr.esClient.BulkInsertLog(log)
	}
	for i := 0; i < 4; i++ {
		log := generator.GenerateAnomaly()
		_ = svr.esClient.BulkInsertLog(log)
	}
	return nil
}

func (svr *messageGenerator) Stop(ctx context.Context) error {
	return nil
}
