package server

import (
	"code.route.com/platform/iam/tracepoints/config"
	"code.route.com/platform/iam/tracepoints/internal"
)

var Component = internal.NewComponent("server",[]config.EnvVar{},newServer)
