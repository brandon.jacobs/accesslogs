package internal

import "time"

//WGS84 Longitude,Latitude is the format we will use here
type AccessLog struct {
	Time time.Time `json:"logged_at_utc"`
	UserId string `json:"user_id"`
	SessionId string `json:"session_id"`
	Latitude string `json:"lat"`
	Longitude string `json:"lon"`
}
