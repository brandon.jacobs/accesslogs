package generator

import (
	"code.route.com/platform/iam/tracepoints/internal"
	"fmt"
	"math/rand"
	"time"
)
const bottomRightLat = 40.043413
const bottomRightLon = -111.519123
const topLeftLat = 40.833272
const topLeftLon = -112.090892

const anomalybottomRightLat = 50.00
const anomalybottomRightLon = 50.32
const anomalytopLeftLat = 57.72
const anomalytopLeftLon = 27.00

var minTime = time.Date(2020,6,1,1,1,1,1,time.UTC).Unix()
var maxTime = time.Now().UTC().Unix()
var userId = "AXXDEDEOIDLEDAD"
var sessionId = "Sexode34930slxe92dl230a11"
func GenerateRandomAccessLog() internal.AccessLog {
	time := generateTimestamp()
	long,lat := generateRandomLatLon()
	return internal.AccessLog{
		Time:      time,
		UserId:    userId,
		SessionId: sessionId,
		Latitude:  lat,
		Longitude: long,
	}
}

func GenerateAnomaly() internal.AccessLog{
	time := generateTimestamp()
	lon, lat := generateAnomalyLatLon()
	return internal.AccessLog{
		Time: time,
		UserId: userId,
		SessionId: sessionId,
		Latitude: lat,
		Longitude: lon,
	}
}

func generateTimestamp() time.Time {
	delta := maxTime - minTime
	sec := rand.Int63n(delta) + minTime
	return time.Unix(sec,0)
}

func generateAnomalyLatLon() (long string, lat string) {
	rLat := anomalybottomRightLat + rand.Float64() * (anomalytopLeftLat-anomalybottomRightLat)
	rLon := anomalybottomRightLon + rand.Float64() * (anomalytopLeftLon-anomalybottomRightLon)
	return fmt.Sprintf("%f",rLon),fmt.Sprintf("%f",rLat)
}

func generateRandomLatLon() (long string,lat string){
	rLat := bottomRightLat + rand.Float64() * (topLeftLat-bottomRightLat)
	rLon := bottomRightLon + rand.Float64() * (topLeftLon-bottomRightLon)
	return fmt.Sprintf("%f",rLon),fmt.Sprintf("%f",rLat)
}
