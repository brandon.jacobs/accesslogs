package generator

import "testing"

func Test_generateRandomLatLon(t *testing.T) {
	for i :=0; i<100; i++{
		lat,lon := generateRandomLatLon()
		t.Logf("lat: %s, lon: %s",lat, lon)
	}
}

func Test_generateTimestamp(t *testing.T){
	for i :=0; i<100; i++{
		time := generateTimestamp()
		t.Logf("time: %v",time)
	}
}
