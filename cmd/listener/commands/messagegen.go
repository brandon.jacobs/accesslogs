package commands

import (
	"code.route.com/platform/iam/tracepoints/internal/app"
	"code.route.com/platform/iam/tracepoints/internal/elasticsearch"
	"code.route.com/platform/iam/tracepoints/internal/logging"
	"code.route.com/platform/iam/tracepoints/internal/server"
)

var Generate = app.ServiceCommand("generate","Generate mock data","Generate real mock data for elasticsearch")

func init(){
	Generate.AddComponent(logging.Component)
	Generate.AddComponent(elasticsearch.Component)
	Generate.AddComponent(server.Component)
	Hackathon.AddCommand(Generate)
}