package main

import (
	"code.route.com/platform/iam/tracepoints/cmd/listener/commands"
	"code.route.com/platform/iam/tracepoints/config"
	"github.com/spf13/viper"
)

func main() {
	// Set build time variables
	cfg := viper.New()
	cfg.Set(config.LogFormat,"console")
	cfg.Set(config.LogLevel,"info")
	// Run the root command
	_ = commands.Hackathon.ToCobra(cfg).Execute()
}